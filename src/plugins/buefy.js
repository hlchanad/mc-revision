import Vue from 'vue';
import Buefy from 'buefy';
import 'buefy/dist/buefy.css';

export const buefy = {
  init() {
    Vue.use(Buefy);
  },
};
