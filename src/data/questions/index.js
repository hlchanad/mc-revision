import questionsJson from './questions.json';
import groupsJson from './question-groups.json';

export const groups = JSON.parse(JSON.stringify(groupsJson));
export const questions = JSON.parse(JSON.stringify(questionsJson));
