import shuffle from 'shuffle-array';

import { config } from '../helpers';

import { questions } from '../data';

export async function getGroups() {
  return [...questions.groups];
}

export async function getQuestionsByGroupId(groupId) {
  let copy = questions.questions.filter(
    (question) => question.groupId === groupId
  );

  return shuffle(
    copy.map((item) => ({ ...item, choices: shuffle(item.choices) }))
  ).slice(0, config.get('app.questions.numberPerGroup'));
}
