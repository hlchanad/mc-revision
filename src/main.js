// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';

import App from './App';
import router from './router';
import { store } from './store';
import * as plugins from './plugins';

Vue.config.productionTip = false;

plugins.buefy.init();

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  i18n: plugins.i18n,
  components: { App },
  template: '<App/>',
});
