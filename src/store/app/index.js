export const app = {
  namespaced: true,
  state: {
    count: 0,
  },
  getters: {},
  actions: {
    addCount({ commit }) {
      commit('ADD_COUNT');
    },
  },
  mutations: {
    ADD_COUNT(state) {
      state.count++;
    },
  },
};
