import Vue from 'vue';
import Router from 'vue-router';

import Groups from '../views/Groups';
import Questions from '../views/Questions';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/groups',
      name: 'groups.index',
      component: Groups,
    },
    {
      path: '/questions',
      name: 'questions.index',
      component: Questions,
    },
    {
      path: '/',
      redirect: '/groups',
    },
    {
      path: '*',
      redirect: '/',
    },
  ],
});
