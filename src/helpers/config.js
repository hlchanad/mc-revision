import * as _ from 'lodash';

import * as config from '../config';

export function get(path, defaultValue) {
  return _.get(config, path, defaultValue);
}
